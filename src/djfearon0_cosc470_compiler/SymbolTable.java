/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package djfearon0_cosc470_compiler;

import java.util.HashMap;

/**
 * The symbol table for a compiler that holds all tokens and their associated
 * types and values.
 *
 * @author Dakota F
 */
public class SymbolTable {

    /**
     *
     */
    public HashMap symbolTable;

    /**
     * Class constructor.&nbsp; Creates a new Symbol Table using Java's HashMap
     * Class.
     */
    public SymbolTable() {
        symbolTable = new HashMap();
    }

    /**
     * Inserts a new token into the Symbol Table.&nbsp;The new
     * symbol object is stored in the symbol table by hashing the name of
     * the symbol for the key.
     *
     * @param symbol the given token
     */
    public void insert(Token symbol) {
        symbolTable.put(symbol.getName(), symbol);
    }

    /**
     * Checks the symbol table to see if a given token is already in the table.
     *
     * @param symbol the token that is being looked for
     * @return Returns true if the symbol is in the table, false otherwise.
     */
    public Token lookup(String symbol) {
        return (Token) symbolTable.get(symbol);
    }
    
    /**
     * Check to see if a given symbol already exists in the table
     * 
     * @param symbol    The symbol that is being searched for
     * @return          boolean that is true if found
     */
    public boolean contains(String symbol) {
        Token s = (Token) symbolTable.get(symbol);
        return s != null;
    }

    /**
     * Remove a symbol from the table
     * 
     * @param s The symbol that is being removed
     */
    public void remove(String s) {
        symbolTable.remove(s);
    }

    /**
     * Return a string of every symbol in the table
     */
    @Override
    public String toString() {
        return symbolTable.toString();
    }
}
