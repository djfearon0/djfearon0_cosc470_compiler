/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package djfearon0_cosc470_compiler;

import java.util.ArrayList;

/**
 * Creates token objects that hold the lexemes retrieved from the scanner.
 * 
 * @author frost
 */
public class Token {

    private final String token;
    private final int line;
    private int code;
    private String value;
    private String type;
    private String name;
    private ArrayList idList;

    /**
     * The token that the scanner reads and submits to the parser.
     * 
     * @param t     The token from the scanner
     * @param num   The line the token was read from
     */
    public Token(String t, int num) {
        token = t;
        line = num;
    }

    /**
     * Return the token string.
     * 
     * @return  The token
     */
    public String getToken() {
        return token;
    }

    /**
     * Return the line that the token was read from.
     * 
     * @return  The line number the token was read on
     */
    public int getLine() {
        return line;
    }

    /**
     * Set the numerical code for parsing.
     * 
     * @param code Set the numerical code for the token
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * Return the numerical code for parsing.
     * 
     * @return Return Return the numerical code for the token
     */
    public int getCode() {
        return code;
    }

    /**
     * Set the data value of the token.
     * 
     * @param v The value to be assigned to the token
     */
    public void setValue(String v) {
        value = v;
    }

    /**
     * Return the assigned value.
     * 
     * @return The value assigned to the token
     */
    public String getValue() {
        return value;
    }

    /**
     * Set the datatype of the token.
     * 
     * @param t The type being assigned to a token
     */
    public void setType(String t) {
        type = t;
    }

    /**
     * Return the assigned datatype.
     * 
     * @return Return the type assigned to a token
     */
    public String getType() {
        if (type == null) {
            return "null";
        } else {
            return type;
        }
    }

    /**
     * Set the name of the token.&nbsp;Typically used in nonTerminals after reduction
     * to track the original token.
     * 
     * @param n The name of the original variable
     */
    public void setName(String n) {
        name = n;
    }

    /**
     * Return the original name of the token.
     * 
     * @return Return the name of the original variable
     */
    public String getName() {
        return name;
    }

    /**
     * Add the previous variable to a reference list.&nbsp;Used to keep track of
     * identifier lists in the type declarations.
     * 
     * @param s The previous token in the identifier list
     */
    public void setPrevToken(String s) {
        if (idList == null) {
            idList = new ArrayList();
        }
        idList.add(s);
    }

    /**
     * Return the list of previous tokens.
     * 
     * @return Return the list of previous tokens in the identifier list
     */
    public ArrayList getPrevToken() {
        if (idList == null) {
            idList = new ArrayList();
        }
        return idList;
    }
}
