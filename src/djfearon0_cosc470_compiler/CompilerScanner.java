/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package djfearon0_cosc470_compiler;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Performs lexical analysis on a file to break up lines of code into separate
 * tokens.&nbsp; Each token is printed to a file prefixed and postfixed with a
 * dollar sign to show spaces before and after the symbol.
 *
 * @author Dakota F
 */
public class CompilerScanner {

    int i = 0;
    int line = 0;

    char[] workingString = {};

    Scanner reader;

    ArrayList tokens = new ArrayList();
    File inputFile;
    String inputLine;
    String comments = "(\\/\\/.*)";
    String token;

    //CompilerParser parser;
    /**
     * Creates a new scanner that accepts a file for scanning.
     *
     * @param filename This is the name of the file that the scanner will
     * perform lexical analysis on.
     * @throws java.io.IOException
     */
    public CompilerScanner(String filename) throws IOException {
        try {
            inputFile = new File(filename);
        } catch (Exception e) {
            System.err.printf("The file '%s' could not be opened.", filename);
        }
        reader = new Scanner(inputFile);
    }

    /**
     * Uses the Java StringTokenizer to break up lines of code into separate
     * tokens.&nbsp; The scanner removes all forms of comments and checks for
     * the use of double symbol operators.
     *
     * @return  Retrieve the next token from the source code
     * @throws FileNotFoundException
     * @throws IOException
     */
    public Token scan() throws FileNotFoundException, IOException {
        
        token = "";

        if (i == workingString.length) {
            workingString = token.toCharArray();
            if (reader.hasNextLine()) {
                getNextLine();
            } else {
                Token t = new Token(token, line);
                return t;
            }
            i = 0;
        }
        if (Character.isWhitespace(workingString[i])) {
            i++;
        }
        if (Character.isDigit(workingString[i]) || (workingString[i] == '-' && Character.isDigit(workingString[i + 1]))) {
            token += workingString[i];
            i++;
            while (Character.isDigit(workingString[i])){
                token += workingString[i];
                i++;
            }
            if (!token.equals("")) {
                Token t = new Token(token, line);
                return t;
            }
        } //Check for keywords or indentifiers
        else if (Character.isAlphabetic(workingString[i]) || workingString[i] == '_') {
            int j = i + 1;
            //Check if token is a user identifier
            if (workingString[i] == Character.toUpperCase(workingString[i]) && workingString[j] == Character.toUpperCase(workingString[j])) {
                token += workingString[i];
                while ((Character.isUpperCase(workingString[j]) || Character.isDigit(workingString[j]) || workingString[j] == '_') && j < workingString.length) {
                    token += workingString[j];
                    j++;
                }
                i = j;
                if (token.length() > 8) {
                    token = token.substring(0, 8);
                }
            } //Check if token is a reserved word
            else if (workingString[i] == Character.toLowerCase(workingString[i]) && workingString[j] == Character.toLowerCase(workingString[j])) {
                token += workingString[i];
                while (Character.isLowerCase(workingString[j]) && j < workingString.length) {
                    token += workingString[j];
                    j++;
                }
                i = j;
            }
            if (!token.equals("")) {
                Token t = new Token(token, line);
                return t;
            }
        } //Check for single operators and terminals
        else {
            token += workingString[i];
            Token t;
            
            //Provide token generation for double symbol comparison operators
            switch (workingString[i]) {
                case '>':
                    i++;
                    if (workingString[i] == '=') {
                        token += workingString[i];
                    }
                    t = new Token(token, line);
                    i++;
                    return t;
                case '=':
                    if (workingString[i+1] == '=') {
                        i++;
                        token += workingString[i];
                    }
                    t = new Token(token, line);
                    i++;
                    return t;
                case '<':
                    i++;
                    if (workingString[i] == '=' || workingString[i] == '>') {
                        token += workingString[i];
                    }
                    t = new Token(token, line);
                    i++;
                    return t;
                default:
                    if (!token.equals("")) {
                        t = new Token(token, line);
                        i++;
                        return t;
                    }
            }
        }

        return null;

    }

    /**
     * Check to see if there is any more code in the source file.
     * 
     * @return boolean based on whether or not there are more tokens in the source file
     */
    public boolean hasMoreTokens() {
        return reader.hasNext();
    }

    /**
     * Retrieve the next line of source code that is not blank.
     */
    private void getNextLine() {
        while (workingString.length == 0) {
            inputLine = reader.nextLine().trim();
            inputLine = inputLine.replaceAll(comments, "").trim();
            workingString = inputLine.toCharArray();
            line++;
        }
    }

}
