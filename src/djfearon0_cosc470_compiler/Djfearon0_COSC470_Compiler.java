/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package djfearon0_cosc470_compiler;

import java.io.IOException;

/**
 *
 * @author Dakota F
 */
public class Djfearon0_COSC470_Compiler {

    /**
     * Main method that runs the compiler.
     * 
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here        
        Compiler compiler = new Compiler(args[0]);
        compiler.compile();
    }
    
}
