/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package djfearon0_cosc470_compiler;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.regex.Pattern;

/**
 * Performs syntactical analysis on the given source code tokens.
 *
 * @author Dakota F
 */
public class Compiler {

    // <editor-fold defaultstate="collapsed" desc="Parse Table"> 
    //The parse table that will drive the parsing process.
    //Tokens are looked up by parseTable[state][symbolCode - 1]
    private final int[][] parseTable = {
        {0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2},
        {-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3},
        {0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99},
        {0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-6, -6, 10, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6},
        {-4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4},
        {0, 0, 0, 0, 12, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8},
        {-5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5},
        {0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-6, -6, 10, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6},
        {0, 0, 21, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 22, 23, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 13, 0, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 31, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 34, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15, -15},
        {0, 0, 0, 0, 0, 0, 35, 0, 0, 0, 0, 0, 0, 36, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12, -12},
        {-14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14, -14},
        {0, 0, 0, 0, 0, 0, 0, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 38, 39, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 42, 43, 44, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 42, 43, 44, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11, -11},
        {0, 0, 21, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 22, 23, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {-9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9},
        {-10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27, -27},
        {-28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28},
        {-29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29, -29},
        {-30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30, -30},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19, -19},
        {-20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20, -20},
        {-21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, -21, 57, 58, 59, 60, 61, 62, 63, 64, -21, -21, -21},
        {-23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, -23, 67, 68, 69},
        {-25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25},
        {0, 0, 0, 0, 71, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 72, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13, -13},
        {-7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 74, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32, -32},
        {-33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33, -33},
        {-34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34, -34},
        {-35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35},
        {-36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36, -36},
        {-37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37, -37},
        {-38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38, -38},
        {-39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39, -39},
        {0, 0, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 42, 43, 44, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 42, 43, 44, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40},
        {-41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41},
        {-42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42, -42},
        {0, 0, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 42, 43, 44, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16},
        {-17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17},
        {0, 0, 21, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 22, 23, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {-31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31},
        {-22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, -22, 63, 64, -22, -22, -22},
        {-24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, -24, 67, 68, 69},
        {-26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26, -26},
        {-18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18}
    };
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="GOTO Table"> 
    //GOTO table that contains the instructions for nonterminal transitions
    private final int[][] goToTable = {
        {3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 25, 26, 27, 28, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 47, 48, 49, 50, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 53, 48, 49, 50, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 25, 0, 54, 28, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 65, 66, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 75, 49, 50, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 76, 50, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 77, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 25, 0, 78, 28, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 66, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    };
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Constant Table"> 
    //Keeps track of Non-Terminal symbols and displays the string structure for the parser in reverse order for the stack
    //When reducing with a rule, the rule from the parse table is parseTable[rule-1][symbolCode]
    private final int[][] constantTable = {
        /*0*/{0, 0, 0},
        /*1*/ {100, 12, 8, 7, 105, 103, 6, 5, 102, 4, 3, 2, 1, 101},
        /*2*/ {101, 1, 9},
        /*3*/ {101, 1, 10},
        /*4*/ {102, 1, 3},
        /*5*/ {102, 3, 3, 11, 102},
        /*6*/ {102, 0, 0},
        /*7*/ {103, 6, 14, 104, 13, 102, 12, 103},
        /*8*/ {103, 0, 0},
        /*9*/ {104, 1, 15},
        /*10*/ {104, 1, 16},
        /*11*/ {105, 3, 7, 106, 6},
        /*12*/ {106, 1, 107},
        /*13*/ {106, 3, 107, 14, 106},
        /*14*/ {107, 1, 108},
        /*15*/ {107, 1, 105},
        /*16*/ {107, 4, 5, 3, 4, 17},
        /*17*/ {107, 4, 5, 3, 4, 18},
        /*18*/ {107, 5, 107, 5, 110, 4, 19},
        /*19*/ {108, 3, 109, 20, 3},
        /*20*/ {109, 1, 110},
        /*21*/ {110, 1, 111},
        /*22*/ {110, 3, 111, 114, 111},
        /*23*/ {111, 1, 112},
        /*24*/ {111, 3, 112, 115, 111},
        /*25*/ {112, 1, 113},
        /*26*/ {112, 3, 113, 116, 112},
        /*27*/ {113, 1, 3},
        /*28*/ {113, 1, 21},
        /*29*/ {113, 1, 22},
        /*30*/ {113, 1, 23},
        /*31*/ {113, 3, 24, 25, 24},
        /*32*/ {114, 1, 26},
        /*33*/ {114, 1, 27},
        /*34*/ {114, 1, 28},
        /*35*/ {114, 1, 29},
        /*36*/ {114, 1, 30},
        /*37*/ {114, 1, 31},
        /*38*/ {115, 1, 32},
        /*39*/ {115, 1, 33},
        /*40*/ {116, 1, 34},
        /*41*/ {116, 1, 35},
        /*42*/ {116, 1, 36}
    };
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Keyword Tree"> 
    //Provides numerical codes for all terminals in the given grammar
    Object[][] keywordTree = {
        {"static", 1},
        {"void", 2},
        {"ID", 3},
        {"(", 4},
        {")", 5},
        {"{", 6},
        {"}", 7},
        {"$", 8},
        {"public", 9},
        {"private", 10},
        {",", 11},
        {"var", 12},
        {":", 13},
        {";", 14},
        {"char", 15},
        {"int", 16},
        {"get", 17},
        {"put", 18},
        {"if", 19},
        {"=", 20},
        {"num", 21},
        {"true", 22},
        {"false", 23},
        {"'", 24},
        {"literal", 25},
        {">", 26},
        {">=", 27},
        {"==", 28},
        {"<=", 29},
        {"<", 30},
        {"<>", 31},
        {"+", 32},
        {"-", 33},
        {"*", 34},
        {"/", 35},
        {"%", 36}
    };
    // </editor-fold>

    /* Global Variable Block Start*/
    //Array for the storage of the numerical equivalents of each token in the tokens ArrayList.
    ArrayList tokenCodes;

    //ArrayList containing the semantic errors that have been accumulated.
    ArrayList errors = new ArrayList();

    //ArrayList containing the intermediate code for mini.
    ArrayList intermediateCode = new ArrayList();

    //Numerical mappings for variable to mini variables.
    ArrayList intermediateVariables = new ArrayList();

    //Lookup structure for looking up the numerical codes for each keyword.
    HashMap keywords;

    //LinkedList that will serve as the stack for the parser.
    LinkedList stack;

    //LinkedList that will serve as the stack for keeping track of which lines conditional statements are generated on.
    LinkedList expStack = new LinkedList();

    //The symbol table for semantic analysis.
    SymbolTable table = new SymbolTable();

    //The error class for creating error messages.
    Errors ErrorGen = new Errors();

    //The state that the parser is currently configured for.
    int currentState = 0;

    //The current line of code that is being processed.
    int line;

    //The number associated with each generated temporary variable.
    int tempCount = 1000;

    //Flag set by to show that the next line is a conditional evalutation.
    boolean expFlag = false;

    //Flag that is set when a semantic error is found.
    boolean semanticError = false;

    //Flag that is set when the first single quote of a character literal appears.
    boolean quote = false;

    //Flag that is set when the character literal is parsed.
    boolean literal = false;

    //The name of the file that contains the source code
    String inputFile;

    //The current line of the generated intermediate code
    int genLine;

    //The file generated that contains the intermediate code for mini and mice
    File outputFile;

    //Print stream for writing the intermediate code to the output file
    PrintStream writer;

    /* Global Variable Block End*/
    /**
     * Creates a new compiler for translation to mini-Assembly.&nbsp;Performs
     * syntax analysis, semantic analysis, and intermediate code
     * generation.&nbsp;Outputs the intermediate code to a file with the same
     * name as the input with the file type .obj.
     *
     * @param filename The name of the file that contains the source code
     * @throws java.io.IOException
     */
    public Compiler(String filename) throws IOException {
        keywords = new HashMap();
        tokenCodes = new ArrayList();
        stack = new LinkedList();
        inputFile = filename;
        genLine = 0;

        Pattern extension = Pattern.compile("\\..*");

        try {
            outputFile = new File(extension.matcher(inputFile).replaceAll(".obj"));
            outputFile.createNewFile();
        } catch (Exception e) {
            System.out.println("The output file could not be created.");
        }

        writer = new PrintStream(outputFile);

        for (Object[] keywordTree1 : keywordTree) {
            keywords.put(keywordTree1[0], keywordTree1[1]);
        }
    }

    /**
     * Fills in the line numbers for all jump statements.
     *
     * @param lineNum The current line of intermediate code that is being
     * accessed
     * @param next The location of the line to jump to
     */
    private void backPatch(int lineNum, int next) {
        String[] quad;
        quad = (String[]) intermediateCode.get(lineNum);
        quad[3] = "#" + next;
    }

    /**
     * Compiles the given source code.&nbsp;Performs syntax and semantic
     * analysis and generates intermediate code.
     *
     * @throws java.io.IOException
     */
    public void compile() throws IOException {
        CompilerScanner scanner = new CompilerScanner(inputFile);
        stack.push(currentState);//Push 0 onto the stack.
        int i = 0;
        Token token = scanner.scan();
        while (!token.getToken().equals("")) {
            line = token.getLine();
            int code = lookupKeyword(token);
            token.setCode(code);
            int instruction = parseTable[currentState][code - 1];
            if (instruction > 0) {
                shift(instruction, token);
                i++;
                token = scanner.scan();
            } else if (instruction < 0) {
                int rule = instruction * -1;
                reduce(rule);
            } else if (instruction == 0) {
                System.err.println(ErrorGen.syntaxError(token));
                try {
                    outputFile.delete();
                } catch (Exception e) {
                    System.err.println(ErrorGen.badFile());
                }
                System.exit(1);
            }
        }
        if (semanticError) {
            for (Object error : errors) {
                System.err.println(error.toString());
            }
            try {
                outputFile.delete();
            } catch (Exception e) {
                System.err.println(ErrorGen.badFile());
            }
        }

        //Perform final reduction and check if result is the start symbol
        reduce(1);
        Token t = (Token) stack.peek();
        if (t.getCode() != 100) {
            try {
                outputFile.delete();
            } catch (Exception e) {
                System.err.println(ErrorGen.badFile());
            }
        } else {
            for (int j = 0; j < intermediateCode.size(); j++) {
                String[] code = (String[]) intermediateCode.get(j);
                writer.printf("%d   %s %s,%s,%s\n", j, code[0], code[1], code[2], code[3]);
            }
            System.out.println("The output file " + outputFile.getName() + " has been created in "
                    + outputFile.getAbsolutePath() + ".");
            if (semanticError) {
                System.err.println(ErrorGen.badFile());
            }
        }
    }

    /**
     * Find the expected symbol that was not found based on the numerical code.
     *
     * @param symbol The numerical code of the expected symbol.
     * @return The symbol that corresponds to the numerical code
     */
    private String errorLookup(int symbol) {
        for (Object[] keywordTree1 : keywordTree) {
            if (keywordTree1[1].equals(symbol)) {
                return keywordTree1[0].toString();
            }
        }
        return "No available symbols";
    }

    /**
     * Creates conditional statements based on the operator provided.
     *
     * @param op The operator for the condition
     * @param se1 The first simple expression
     * @param se2 The second simple expression
     * @param val1 The value if the first simple expression is a character
     * @param val2 The value if the second simple expression is a character
     * @param temp The temporary variable to store the outcome
     */
    private void genIfStatement(String op, Token se1, Token se2, int val1, int val2, String temp) {
        newQuad(op, (se1.getName() != null) ? se1.getName() : "" + val1,
                (se2.getName() != null) ? se2.getName() : "" + val2, "#" + (intermediateCode.size() + 3));
        newQuad("STO", "#1", "", temp);
        newQuad("JMP", "", "", "#" + (intermediateCode.size() + 2));
        newQuad("STO", "#0", "", temp);
        newQuad("JNE", temp, "#0", "");
        if (expFlag == true) {
            expStack.push(intermediateCode.size() - 1);
            expFlag = false;
        }
    }

    /**
     * Handles the lookup of the code and converts it from an object to an
     * integer.
     *
     * @param key The given token for looking up its code.
     * @return Returns the numerical code for a symbol
     */
    private int lookup(String key) {
        String codeStr = keywords.get(key).toString();
        int code = Integer.parseInt(codeStr);
        if (codeStr == null) {
            return -1;
        }
        return code;
    }

    /**
     * Searches the HashMap for the given token and returns the numerical code
     * assigned to each terminal.
     *
     * @param symbol The given token to be converted to a numerical code.
     * @return The numerical code associated with the symbol
     */
    public int lookupKeyword(Token symbol) {
        int code;
        String token = symbol.getToken();

        if (token.equals("if")) {
            expFlag = true;
        } else if (token.equals("'") && literal == false) {
            quote = true;
        } else if (token.equals("'") && literal == true) {
            quote = false;
            literal = false;
        }
        if (token.length() == 1 && token.matches("[a-zA-Z.,?!@`~^#$+-/%=&|_\\\\(){}\\[\\]:;\"]*") && quote == true) {
            literal = true;
            code = lookup("literal");
            symbol.setType("char");
            symbol.setValue(symbol.getToken());
        } else if (token.equals(token.toUpperCase()) && (Character.isAlphabetic(token.charAt(0)) || token.charAt(0) == '_')) {
            code = lookup("ID");
            table.insert(symbol);
        } else if (Character.isDigit(token.charAt(0)) || ((token.length() > 1) && (token.charAt(0) == '-' && Character.isDigit(token.charAt(1))))) {
            code = lookup("num");
            symbol.setType("int");
            symbol.setValue(symbol.getToken());
        } else {
            code = lookup(token);
            if (code == -1) {
                errors.add(ErrorGen.syntaxError(symbol));
            }
        }
        return code;
    }
    
    /**
     * Generates a new temporary variable for the intermediate code starting
     * from the number 1000.
     *
     * @return A new temporary variable for intermediate code
     */
    private String newTemp() {
        String temp = "" + tempCount;
        tempCount++;
        return temp;
    }

    /**
     * Generates a new quadruple for the intermediate code of the form (OPCODE,
     * Value, Value, Storage Address).
     *
     * @param op The operation for the line to perform
     * @param val1 The first value (optional)
     * @param val2 The second value (optional)
     * @param storage The location that values are stored
     */
    private void newQuad(String op, String val1, String val2, String storage) {
        String[] intCode = new String[4];
        intCode[0] = op;
        intCode[1] = val1;
        intCode[2] = val2;
        intCode[3] = storage;
        intermediateCode.add(intCode);
    }

    /* Parser Actions Start*/
    /**
     * Reduces a set of terminals using a production rule to produce a single
     * nonterminal that represents the terminals.
     *
     * @param rule The position in the constant array that contains the
     * reduction rule.
     */
    private void reduce(int rule) {
        //Set up code to reduce and compare to constant table.
        int nonTerminal = constantTable[rule][0];
        int ruleSize = constantTable[rule][1] + 2;
        Token nonT = new Token("" + nonTerminal, line);
        semanticsAndCodeGen(nonT, rule);
        Token token;
        for (int i = 2; i < ruleSize; i++) {
            stack.pop(); //Pop off state
            token = (Token) stack.pop();//Pop off top symbol
            int symbol = token.getCode();
            line = token.getLine();
            if (symbol != constantTable[rule][i]) {
                System.err.println(ErrorGen.unexpectedToken(errorLookup(constantTable[rule][i]),
                        errorLookup(symbol), line));
            }
        }
        int goTo = Integer.parseInt(stack.peek().toString());
        currentState = goToTable[goTo][nonTerminal - 100];
        nonT.setCode(nonTerminal);
        stack.push(nonT);
        if (rule != 1) {
            stack.push(currentState);
        }
    }

    /**
     * Push a terminal symbol onto the stack for reduction later.
     *
     * @param state The current state.
     * @param symbol The symbol seen in the current state.
     */
    private void shift(int state, Token symbol) {
        stack.push(symbol);
        stack.push(state);
        currentState = state;
    }
    /* Parser Actions End */

    /**
     * Provides an Attribute Grammar to perform semantic analysis based on the
     * current production rule.
     *
     * @param nonTerminal The nonterminal of this production rule
     * @param rule The current rule being used in reduction
     */
    public void semanticsAndCodeGen(Token nonTerminal, int rule) {
        switch (rule) {

            // start --> access static void ID ( identifier_list ) { declarations compound_statement } $
            case 1:
                newQuad("NOP", "", "", "");
                newQuad("HLT", "", "", "");
                break;

            // identifer_list --> ID
            case 4:
                Token var = (Token) stack.get(1);
                var.setName(var.getToken());
                nonTerminal.setType(var.getType());
                nonTerminal.setName(var.getToken());
                table.insert(var);
                if (!intermediateVariables.contains(var.getToken())) {
                    intermediateVariables.add(var.getToken());
                }
                break;

            // identifer_list --> identifier_list, ID
            case 5:
                var = (Token) stack.get(1);
                Token prev = (Token) stack.get(5);
                prev.setType(var.getType());
                nonTerminal.setPrevToken(prev.getName());
                ArrayList idList = prev.getPrevToken();
                if (!idList.isEmpty()) {
                    for (Object token : idList) {
                        nonTerminal.setPrevToken(token.toString());
                    }
                }
                nonTerminal.setType(var.getType());
                nonTerminal.setName(var.getToken());
                table.insert(var);
                table.insert(prev);

                if (!intermediateVariables.contains(var.getToken())) {
                    intermediateVariables.add(var.getToken());
                }
                break;

            // declarations --> declarations var identifier_list : type ;
            case 7:
                Token type = (Token) stack.get(3);
                var = (Token) stack.get(7);
                var.setType(type.getType());
                table.insert(var);
                idList = var.getPrevToken();
                if (!idList.isEmpty()) {
                    for (int i = 0; i < idList.size(); i++) {
                        nonTerminal.setPrevToken(idList.get(i).toString());
                        Token p = (Token) table.lookup(idList.get(i).toString());
                        p.setType(type.getType());
                        table.insert(p);
                    }
                }
                nonTerminal.setType(var.getType());
                break;

            // type --> char
            case 9:
                type = (Token) stack.get(1);
                nonTerminal.setType(type.getToken());
                nonTerminal.setName(type.getName());
                table.insert(nonTerminal);
                break;

            // type --> int
            case 10:
                type = (Token) stack.get(1);
                nonTerminal.setType(type.getToken());
                nonTerminal.setName(type.getName());
                table.insert(nonTerminal);
                break;

            // statement --> lefthandside
            case 14:
                Token statement = (Token) stack.get(1);
                nonTerminal.setType(statement.getType());
                nonTerminal.setValue(statement.getValue());
                break;

            // statement --> get ( ID )
            case 16:
                Token receiver = (Token) stack.get(3);
                newQuad("SYS", "#1", "", "" + intermediateVariables.indexOf(receiver.getToken()));
                break;

            // statement --> put ( ID )
            case 17:
                Token output = (Token) stack.get(3);
                Token out = table.lookup(output.getToken());

                if (out.getValue() == null) {
                    errors.add(ErrorGen.notInitialized(out.getName(), line));
                }

                if (out.getType().equals("int")) {
                    newQuad("SYS", "#-1", "" + intermediateVariables.indexOf(output.getToken()), "");
                } else if (out.getType().equals("char")) {
                    if (out.getValue() != null) {
                        int ch = (int) out.getValue().charAt(0);
                        switch (ch) {
                            case (int) '\\':
                                newQuad("SYS", "#0", "", "");
                                break;
                            case (int) '_':
                                newQuad("SYS", "#-2", "#32", "");
                                break;
                            default:
                                newQuad("SYS", "#-2", "#" + ch, "");
                                break;
                        }
                    } else {
                        errors.add(ErrorGen.nullPointer(out.getToken(), line));
                    }
                }
                break;

            // statement --> if ( expression ) statement
            case 18:
                //This is the if statement rule
                if (!expStack.isEmpty()) {
                    int popped = (int) expStack.pop();
                    backPatch(popped, intermediateCode.size());
                }
                break;

            // lefthandside --> ID = righthandside
            case 19:
                Token rhs = (Token) stack.get(1);
                Token id = (Token) stack.get(5);
                String name = id.getToken();
                Token variable = (Token) table.lookup(name);
                if (!variable.getType().equals(rhs.getType())) {
                    errors.add(ErrorGen.badAssigment(rhs.getType(), variable.getType(), name, line));
                    semanticError = true;
                } else if (variable.getType().equals("null")) {
                    errors.add(ErrorGen.notDeclared(name, line));
                    semanticError = true;
                } else if (variable.getType().equals(rhs.getType())) {
                    variable.setValue(rhs.getValue());
                    variable.setName(name);
                    table.insert(variable);
                } else {
                    errors.add(ErrorGen.badAssigment(rhs.getType(), variable.getType(), name, line));
                    semanticError = true;
                }

                int value = 0;

                if (rhs.getType().equals("int")) {
                    value = Integer.parseInt(rhs.getValue());
                } else if (rhs.getType().equals("char")) {
                    value = (int) rhs.getValue().charAt(0);
                }
                newQuad("STO", (rhs.getName() != null) ? rhs.getName() : "#" + value, "", "" + intermediateVariables.indexOf(id.getToken()));
                break;

            // righthandside --> expression
            case 20:
                Token se = (Token) stack.get(1);
                nonTerminal.setType(se.getType());
                nonTerminal.setValue(se.getValue());
                nonTerminal.setName(se.getName());
                break;

            // expression --> simple_expression
            case 21:
                se = (Token) stack.get(1);
                nonTerminal.setType(se.getType());
                nonTerminal.setValue(se.getValue());
                nonTerminal.setName(se.getName());
                if (expFlag == true) {
                    expStack.push(intermediateCode.size() - 3);
                    expFlag = false;
                }
                break;

            // expression --> simple_expression relop simple_expression
            case 22:
                Token se2 = (Token) stack.get(1);
                Token relop = (Token) stack.get(3);
                Token se1 = (Token) stack.get(5);

                String operator = relop.getName();
                int val1 = 0;
                int val2 = 0;

                if (se1.getValue() == null) {
                    errors.add(ErrorGen.notInitialized(se1.getName(), line));
                    semanticError = true;
                } else if (se2.getValue() == null) {
                    errors.add(ErrorGen.notInitialized(se2.getName(), line));
                    semanticError = true;
                } else if (!se1.getType().equals(se2.getType())) {
                    errors.add(ErrorGen.badComp(se1.getType(), se2.getType(), line));
                    semanticError = true;
                } else if (se1.getType().equals("int") && se2.getType().equals("int")) {
                    val1 = Integer.parseInt(se1.getValue());
                    val2 = Integer.parseInt(se2.getValue());
                } else if (se1.getType().equals("char") && se2.getType().equals("char")) {
                    val1 = (int) se1.getValue().charAt(0);
                    val2 = (int) se2.getValue().charAt(0);
                }

                if (se1.getType().equals(se2.getType())) {
                    String temp = newTemp();
                    switch (operator) {
                        case ">": {
                            boolean val = val1 > val2;
                            nonTerminal.setValue("" + val);
                            nonTerminal.setName(temp);
                            genIfStatement("JGT", se1, se2, val1, val2, temp);
                            break;
                        }
                        case ">=": {
                            boolean val = val1 >= val2;
                            nonTerminal.setValue("" + val);
                            nonTerminal.setName(temp);
                            genIfStatement("JGE", se1, se2, val1, val2, temp);
                            break;
                        }
                        case "==": {
                            boolean val = val1 == val2;
                            nonTerminal.setValue("" + val);
                            nonTerminal.setName(temp);
                            genIfStatement("JEQ", se1, se2, val1, val2, temp);
                            break;
                        }
                        case "<=": {
                            boolean val = val1 <= val2;
                            nonTerminal.setValue("" + val);
                            nonTerminal.setName(temp);
                            genIfStatement("JLE", se1, se2, val1, val2, temp);
                            break;
                        }
                        case "<": {
                            boolean val = val1 < val2;
                            nonTerminal.setValue("" + val);
                            nonTerminal.setName(temp);
                            genIfStatement("JLT", se1, se2, val1, val2, temp);
                            break;
                        }
                        case "<>": {
                            boolean val = val1 != val2;
                            nonTerminal.setValue("" + val);
                            nonTerminal.setName(temp);
                            genIfStatement("JNE", se1, se2, val1, val2, temp);
                            break;
                        }
                        default:
                            break;
                    }
                } else {
                    errors.add(ErrorGen.badComp(se1.getType(), se2.getType(), line));
                    semanticError = true;
                }
                break;

            // simple_expression --> term
            case 23:
                Token term = (Token) stack.get(1);
                nonTerminal.setType(term.getType());
                nonTerminal.setValue(term.getValue());
                nonTerminal.setName(term.getName());
                break;

            // simple_expression --> simple_expression addop term
            case 24:
                term = (Token) stack.get(1);
                Token op = (Token) stack.get(3);
                Token simple_e = (Token) stack.get(5);
                String temp = newTemp();
                if (simple_e.getType().equals(term.getType())) {
                    if (term.getValue() == null) {
                        errors.add(ErrorGen.notInitialized("" + intermediateVariables.get(Integer.parseInt(term.getName())), line));
                        semanticError = true;
                        break;
                    } else if (simple_e.getValue() == null) {
                        errors.add(ErrorGen.notInitialized("" + intermediateVariables.get(Integer.parseInt(simple_e.getName())), line));
                        semanticError = true;
                        break;
                    }
                    if (op.getName().equals("+")) {
                        int val = Integer.parseInt(term.getValue()) + Integer.parseInt(simple_e.getValue());
                        nonTerminal.setType("int");
                        nonTerminal.setValue("" + val);
                        nonTerminal.setName(temp);
                        newQuad("ADD", (term.getName() != null) ? term.getName() : "#" + term.getValue(),
                                (simple_e.getName() != null) ? simple_e.getName() : "#" + simple_e.getValue(), temp);
                    } else if (op.getName().equals("-")) {
                        int val = Integer.parseInt(simple_e.getValue()) - Integer.parseInt(term.getValue());
                        nonTerminal.setType("int");
                        nonTerminal.setValue("" + val);
                        nonTerminal.setName(temp);
                        newQuad("SUB", (term.getName() != null) ? term.getName() : "#" + term.getValue(),
                                (simple_e.getName() != null) ? simple_e.getName() : "#" + simple_e.getValue(), temp);
                    }
                } else {
                    errors.add(ErrorGen.badExpr(term.getType(), simple_e.getType(), line));
                    semanticError = true;
                }
                break;

            // term --> factor
            case 25:
                Token factor = (Token) stack.get(1);
                nonTerminal.setType(factor.getType());
                nonTerminal.setValue(factor.getValue());
                nonTerminal.setName(factor.getName());
                break;

            // term --> term mulop factor
            case 26:
                //Case for term mulop factor
                factor = (Token) stack.get(1);
                op = (Token) stack.get(3);
                term = (Token) stack.get(5);
                temp = newTemp();
                if (term.getValue() == null || term.getType() == null) {
                    errors.add(ErrorGen.notInitialized("" + intermediateVariables.get(Integer.parseInt(term.getName())), line));
                    semanticError = true;
                } else if (factor.getValue() == null || factor.getType() == null) {
                    errors.add(ErrorGen.notInitialized("" + intermediateVariables.get(Integer.parseInt(factor.getName())), line));
                    semanticError = true;
                }
                if (term.getType().equals(factor.getType())) {
                    int val = 0;
                    switch (op.getName()) {
                        case "*":
                            val = Integer.parseInt(term.getValue()) * Integer.parseInt(factor.getValue());
                            nonTerminal.setType("int");
                            nonTerminal.setValue("" + val);
                            nonTerminal.setName(temp);
                            newQuad("MUL", (term.getName() != null) ? term.getName() : "#" + term.getValue(),
                                    (factor.getName() != null) ? factor.getName() : "#" + factor.getValue(), temp);
                            break;
                        case "/":
                            if (Integer.parseInt(factor.getValue()) != 0) {
                                val = Integer.parseInt(term.getValue()) / Integer.parseInt(factor.getValue());
                            } else {
                                errors.add(ErrorGen.divByZero(line));
                                semanticError = true;
                            }
                            nonTerminal.setType("int");
                            nonTerminal.setValue("" + val);
                            newQuad("DIV", (term.getName() != null) ? term.getName() : "#" + term.getValue(),
                                    (factor.getName() != null) ? factor.getName() : "#" + factor.getValue(), temp);
                            break;
                        case "%":
                            val = Integer.parseInt(term.getValue()) * Integer.parseInt(factor.getValue());
                            nonTerminal.setType("int");
                            nonTerminal.setValue("" + val);
                            newQuad("MOD", (term.getName() != null) ? term.getName() : "#" + term.getValue(),
                                    (factor.getName() != null) ? factor.getName() : "#" + factor.getValue(), temp);
                            break;
                        default:
                            break;
                    }
                } else {
                    errors.add(ErrorGen.badExpr(term.getType(), factor.getType(), line));
                    semanticError = true;
                }
                break;

            // factor --> ID
            case 27:
                var = (Token) stack.get(1);
                if (table.lookup(var.getToken()) != null) {
                    var = table.lookup(var.getToken());
                } else {
                    errors.add(ErrorGen.notDeclared(var.getName(), line));
                    semanticError = true;
                }
                nonTerminal.setType(var.getType());
                nonTerminal.setValue(var.getValue());
                nonTerminal.setName("" + intermediateVariables.indexOf(var.getName()));
                break;

            // factor --> num
            case 28:
                Token num = (Token) stack.get(1);
                nonTerminal.setType(num.getType());
                nonTerminal.setValue(num.getValue());
                break;

            // factor --> true
            case 29:
                temp = newTemp();
                newQuad("JNE", "#1", "#0", "#" + (intermediateCode.size() + 3));
                newQuad("STO", "#1", "", temp);
                newQuad("JMP", "", "", "#" + (intermediateCode.size() + 2));
                newQuad("STO", "#0", "", temp);
                newQuad("JNE", temp, "#0", "");
                if (expFlag == true) {
                    expStack.push(intermediateCode.size() - 1);
                    expFlag = false;
                }
                break;

            // factor --> false
            case 30:
                temp = newTemp();
                newQuad("JNE", "#0", "#0", "#" + (intermediateCode.size() + 3));
                newQuad("STO", "#1", "", temp);
                newQuad("JMP", "", "", "#" + (intermediateCode.size() + 2));
                newQuad("STO", "#0", "", temp);
                newQuad("JNE", temp, "#0", "");
                if (expFlag == true) {
                    expStack.push(intermediateCode.size() - 1);
                    expFlag = false;
                }
                break;

            // factor --> ' literal '
            case 31:
                Token ch = (Token) stack.get(3);
                nonTerminal.setValue(ch.getToken());
                nonTerminal.setType(ch.getType());
                nonTerminal.setName(ch.getName());
                break;

            /* This block of cases contains all operators */
            // relop --> >
            case 32:// Greater Than
                op = (Token) stack.get(1);
                nonTerminal.setName(op.getToken());
                break;

            // relop --> >=
            case 33:// Greater Than or Equal To
                op = (Token) stack.get(1);
                nonTerminal.setName(op.getToken());
                break;

            // relop --> ==
            case 34:// Equal To
                op = (Token) stack.get(1);
                nonTerminal.setName(op.getToken());
                break;

            // relop --> <=
            case 35:// Less Than or Equal To
                op = (Token) stack.get(1);
                nonTerminal.setName(op.getToken());
                break;

            // relop --> <
            case 36:// Less Than
                op = (Token) stack.get(1);
                nonTerminal.setName(op.getToken());
                break;

            // relop --> <>
            case 37:// Not Equal To
                op = (Token) stack.get(1);
                nonTerminal.setName(op.getToken());
                break;

            // addop --> +
            case 38:// Addition
                op = (Token) stack.get(1);
                nonTerminal.setName(op.getToken());
                break;

            // addop --> -
            case 39:// Subtraction
                op = (Token) stack.get(1);
                nonTerminal.setName(op.getToken());
                break;

            // mulop --> *
            case 40:// Multiplication
                op = (Token) stack.get(1);
                nonTerminal.setName(op.getToken());
                break;

            // mulop --> /
            case 41:// Division
                op = (Token) stack.get(1);
                nonTerminal.setName(op.getToken());
                break;

            // mulop --> %
            case 42:// Modulus
                op = (Token) stack.get(1);
                nonTerminal.setName(op.getToken());
                break;
            /* The above block of cases contains all operators */

            default:
                //No Attribute Grammar rule associated with the Production rule
                break;
        }
    }
}
