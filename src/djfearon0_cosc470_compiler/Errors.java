/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package djfearon0_cosc470_compiler;

/**
 *
 * @author frost
 */
public class Errors {
    
    /**
     * Returns an error when an assignment statement contains mismatched types.
     * 
     * @param t1    The value type being assigned
     * @param t2    The variable type receiving the value
     * @param name  The variable name
     * @param line  The line of source code the assignment occurs on
     * @return      Error message
     */
    public String badAssigment(String t1, String t2, String name, int line){
        return "Bad Assignment:  Value of type " + t1 + " can not be assigned to "
                + t2 + " " + name + " on line " + line + ".";
    }
    
    /**
     * Returns an error when a comparison expression contains mismatched types.
     * 
     * @param t1    The type on the lefthand side
     * @param t2    The type on the righthand side
     * @param line  The line of the source code the comparison occurs on
     * @return      Error message
     */
    public String badComp(String t1, String t2, int line){
        return "Bad Comparison:  Type " + t1 + " cannot be compared to type " + t2 + " on line " + line;
    }
    
    /**
     * Returns an error when an expression contains mismatched types.
     * 
     * @param t1    The type on the lefthand side
     * @param t2    The type on the righthand side
     * @param lineq The line of the source code the expression occurs on
     * @return      Error message
     */
    public String badExpr(String t1, String t2, int line){
        return "Bad Expression:  The expression on line " + line
                            + " with " + t1
                            + " and " + t2 + " cannot be evaluated.";
    }
    
    /**
     * Returns an error that the output file was generated but contained errors.
     * 
     * @return      Error message
     */
    public String badFile(){
        return "Compilation Error:  The output file contains errors but could not be removed.";
    }
    
    /**
     * Returns an error message stating that an arithmetic statement contains
     * division by zero.
     * 
     * @param line  The line of the code that division by zero occurs on
     * @return      Error message
     */
    public String divByZero(int line){
        return "Divide by Zero:  The expression on line " + line + " contains division by 0.";
    }
    
    /**
     * Returns an error stating that a variable was not declared.
     * 
     * @param name  The name of the variable that has not been declared
     * @param line  The line that the code attempted to access the variable
     * @return      Error message
     */
    public String notDeclared(String name, int line){
        return "Undeclared Variable:  " + name + " on line " + line + " has not been declared.";
    }
    
    /**
     * Returns an error when a variable that is called has not been initialized.
     * 
     * @param name  The name of the variable that has not been initialized
     * @param line  The line that the code attempted to access the variable
     * @return      Error message
     */
    public String notInitialized(String name, int line){
        return "Uninitialized Variable:  " + name + " on line " + line + " was not initialized.";
    }
    
    /**
     * Returns an error when a variable that is called does not have a value.
     * 
     * @param token The token that has a null value
     * @param line  The line that access was attempted on the token
     * @return      Error message
     */
    public String nullPointer(String token, int line){
        return "Null Pointer:  Attempted access of a null value in " + token + " on line " + line;
    }
    
    /**
     * Returns an error for syntax errors showing that a token that was not expected
     * was found.
     * 
     * @param token The token that was not expected in syntax analysis
     * @return      Error message
     */
    public String syntaxError(Token token){
        return "Unexpected Symbol: \"" + token.getToken() + "\" on line " + token.getLine() + ".";
    }
    
    /**
     * Returns a syntax error message stating what symbol was expected, what was
     * found, and on which line the error occurred.
     * 
     * @param expected  The token that was expected in syntax analysis
     * @param received  The token that was not expected in syntax analysis
     * @param line      The line that the error occurred on
     * @return          Error message
     */
    public String unexpectedToken(String expected, String received, int line){
        return "Error on line " + line + ": expected " + expected + ", but found "
                        + received;
    }
}
